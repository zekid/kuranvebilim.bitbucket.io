---
layout: post
title:  "32 Resimde Budizm Nedir?"
author: admin
categories: [ Budizm ]
image: assets/images/20-630x461.jpg
tags: [Budizm]
keywords: Budizm
comments: false
published: true
lang: tr
encoding: UTF-8
---

  
1-Herşeye güç yetiren Yüce Allah, yalnızca Kendisi’ne kulluk edilmesini emretmiştir: Ey iman eden kullarım, şüphesiz Benim arzım geniştir; artık yalnızca Bana ibadet edin. (Ankebut Suresi, 56)

  

[![kuran](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/1-2.jpg "kuran")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/1-2.jpg)

2-İnsanların dünya üzerinde gerçek huzur ve mutluluğu bulmalarının, her türlü kötülükten, acımasızlıktan, karamsarlıktan ve mutsuzluktan kurtulmalarının tek yolu, Yaratıcımız olan Allah’a teslim olmak ve O’nun razı olacağı gibi bir hayat sürmektir. Yerlerin ve göklerin tek sahibi olan Rabbimiz, tüm insanların kurtuluş yolunun bir hidayet rehberi olarak indirdiği Kuran’a sarılmak olduğunu bildirmiştir. Allah İbrahim Suresi’nde şu şekilde buyurmaktadır: “…Bu bir kitaptır ki Rabbinin izniyle insanları karanlıklardan nura, O güçlü ve övgüye layık olanın yoluna çıkarman için sana indirdik.” (İbrahim Suresi,1)

[![mutlu](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/2-2.jpg "mutlu")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/2-2.jpg)

3-Buda’yı bir ilah gibi görüp (Allah’ı tenzih ederiz), onu herşeyi gören, işiten, insana güç veren, kızan, hoşgören bir put olarak kabul eden kişiler iyice düşünüp, bu sapkın anlayıştan uzaklaşmalıdırlar. Ya da Karma gibi asılsız inançlara kapılarak, ahiretin varlığını reddedenler, akıllarını kullanarak bu yanılgıdan kurtulmalıdırlar.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/3-2.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/3-2.jpg)

4-Buda’yı Allah’a ortak koşan Budistler ve bir özenti ya da ilgi çekme uğruna Budizmi seçenler nasıl büyük bir aldanış içinde olduklarının farkında değildirler. Ahiret, cennet, cehennem inancına sahip olmadıkları ve bu konuda hiçbir şekilde düşünmedikleri için yaptıklarının Allah katında karşılığı olabileceğini de akıllarına getirmemektedirler.

[![dövme](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/4-2.jpg "dövme")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/4-2.jpg)

5-Budizm, materyalist Batı dünyası tarafından da destek görmektedir. Materyalizm ve ateizmin hızla çöktüğü günümüzde, insanların hak dine yönelmelerini önleyebilmek için, Budizm başta olmak üzere pek çok batıl inanışın propagandası yapılmaktadır. Bu içi boş öğretilerin gerçek yüzlerini anlamak içinse, sadece biraz düşünmek, bunları akıl ve mantık süzgecinden geçirmek yeterli olacaktır.

[![budizm inancı](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/5-2.jpg "budizm inancı")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/5-2.jpg)

6-Türler arasında “geçişlilik” öngören ve insanı da hayvanlar arasında gören bu Budist anlayış, Darwin’in evrim teorisiyle önemli bir paralellik taşımaktadır. Bir araştırmacı, Budizm’in evrimle olan bu ilişkisini şöyle tarif eder: “Aslında Budist felsefe, evrimin gerçekleşmiş olmasını gerekli kılar çünkü herşeyi dönüşken varlıklar olarak görmekte, sürekli olarak yoktan var olduğunu, bir süre varlığını sürdürdüğünü ve sonra kaybolduğunu savunmaktadır. Türlerin değişmeden sabit kalması, Budist ontoloji ile uyumlu olmayacaktır.”

[![kaplan ve insan](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/6-2.jpg "kaplan ve insan")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/6-2.jpg)

7-Budizm ateist bir dindir; Allah’ın varlığını kabul etmez. Ahireti, cennet ve cehennemi kabul etmez. İnsanın hayvanlardan farklı bir ruhu olmadığını varsayar ve dahası Karma inancı çerçevesinde doğada sürekli bir dönüşüm olduğuna inanır. Budizm’e göre bir balık “sonraki yaşamında” bir memeli olarak dünyaya gelebilir, bir insan “sonraki yaşamında” bir solucan olabilir.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/7-2.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/7-2.jpg)

8-Budizm’de ahiret, cennet ve cehennem inancı da yer almamaktadır. Bazı Budist kaynaklarda ise ölüm sonrası hayatla ilgili olarak şu bilgilerin verildiği görülmektedir: “Yeniden doğum, ister cennette ister cehennemin muhtelif katmanlarından birinde gerçekleşmiş olsun, söz konusu bu mekanlardaki varoluşlar aynen yeryüzündekiler gibi geçicidir, ebedi değildir. Ferdin bu mekanlardaki kalış süresi, Hinduizm’de olduğu gibi, onun yeryüzünde iken yaptığı iyilik ve kötülüğün miktarına bağlıdır. Belirlenen sürenin tamamlanmasından sonra yeniden yeryüzüne dönülecektir. Cennet ve cehennem, ferdin yeryüzündeki fiillerinin karşılığını gördüğü geçici varoluş katmanlarından başka bir şey değildir.”

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/8-2.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/8-2.jpg)

9-Karma öğretisi, yapılan her türlü eylemin tepkilerinin er ya da geç yapan kişiye geri döneceğini ve bunun sözde “bir sonraki yaşamını” etkileyeceğini varsayar. Bu batıl inanışa göre insan dünyaya sürekli yeniden gelmektedir ve bir sonraki yaşamında bir önceki yaşamında yaptıklarının sonucunu alacaktır. Allah’ın varlığını inkar eden Budizm’e göre de herşeyi idare eden yegane kuvvet karmadır. (Allah’ı tenzih ederiz.)

[![kitap](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/9-2.jpg "kitap")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/9-2.jpg)

10-Budistler günde tek bir öğün yemek yerler ve bu da öğleden önce olmak zorundadır. Öğleden sonra bir ertesi güne kadar bir şey yemek yasaktır. Yemek genellikle ekmek, pirinç ve baharattan oluşur. İçecekleri ise, su veya pirinç sütüdür. Başka yiyecekler “lüks” sayılır ve yasaklanır, hatta ilaçlar bile yasaktır. Et, balık ve meyve gibi yiyecekler sadece eğer rahip hasta ise ve o da bir üst rahibin daveti üzerine yenir.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/10.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/10.jpg)

11-Evlilik ve her türlü aile yaşamı da yine rahiplere yasaktır. Her rahibin sadece tek bir elbisesi olabilir, bu da kalitesiz sarı kumaştan olmalıdır. Bu giysinin yanında başka tek eşyaları da; uyku için kullanabilecekleri sert bir yatak, saçlarını kazımak için ustura, iğne, matara ve dilenmek için bir kaptır.

[![budist rahip](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/11.jpg "budist rahip")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/11.jpg)

12-Budist rahipler için hayat türlü zorluklarla doludur. Çalışamazlar ve mülk sahibi olamazlar. Günlük yiyeceklerini, halk arasında ellerinde bir kap ile gezip dilenerek gidermek zorundadırlar. Hatta bu nedenle Budist rahiplere halk arasında “bhikkus” (dilenciler) ismi verilmiştir.

[![budist rahip](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/12-1.jpg "budist rahip")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/12-1.jpg)

13-Budizmin insanın tüm arzularını yok etmeye çalışması ise bir başka dar görüşlülüktür. Allah dünyadaki nimetleri insanların yararlanması, zevk alması ve karşılığında da Kendisine şükretmesi için yaratmıştır. Bu nedenle İslam insanlara arzularını köreltmelerini, kendilerine acı ve ızdırap çektirmelerini emretmez. Aksine, dünyadaki güzelliklerden (çirkin ve kötü olan haram davranışlar dışında) yararlanmalarını, kendilerine gereksiz yere sınırlama ve baskı yapmamalarını, kendilerine acı çektirmemelerini emreder.

[![budist rahip](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/13-1.jpg "budist rahip")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/13-1.jpg)

14-Budistler Buda’yı “herşeyi gören” ve “herşeyi bilen” olarak kabul etmektedirler. Budizmin hakim olduğu ülkelerin dört bir yanında görülen Buda heykelleri, tapınakların üzerine yerleştirilen Buda’nın gözleri hep bu sapkın anlayışı ifade etmektedir. Budistler Buda’nın her an kendilerini gördüğünü düşünmektedirler. Bu nedenle de evlerini Buda heykelleriyle doldurmakta, bunların önünde saygı gösterilerinde bulunmaktadırlar. Buda’nın taştan, tahtadan yapılmış gözleriyle kendilerini göreceğine, tahtadan kulaklarıyla kendilerini işiteceğine inanarak hem çok büyük bir günah işlemekte, hem de akla aykırı bir tavır sergilemektedirler.

[![budist  çocuk](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/14-1.jpg "budist  çocuk")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/14-1.jpg)

15-Budist yazıtlarda bir Yaratıcı’nın varlığına, kainatın, canlıların ve evrenin nasıl ortaya çıktığına hiçbir şekilde değinilmediği gibi, hiçbir Budist metinde de, kainatın yoktan nasıl var edildiği, canlılığın nasıl ortaya çıktığı ve dünya üzerindeki eşsiz yaratılış delillerinin nasıl var olduğu anlatılmaz. Budistlerin aldanışına göre bu konuda düşünmek dahi gereksizdir. Budist metinlere göre hayatta tek önemli olan şey arzuları yok etmek, ızdıraplardan kurtulmak ve Buda’ya saygı göstermektir.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/15-2.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/15-2.jpg)

16-Budizm, Allah’ın varlığını inkar eden, sadece insanın bazı ahlaki yönlerden gelişimini ve dünyaya ait ızdıraplarından kurtulmasını temel alan ateist bir felsefedir. Bu din hiçbir akılcı ve bilimsel dayanağı bulunmayan birer dogma olan karma ve reenkarnasyon inancı (insanın dünyaya sürekli geldiği, bir önceki hayatındaki davranışlara göre bir sonraki hayatının şekillendiği düşüncesi) üzerine kurulmuştur.

[![budist rahipler](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/16-1.jpg "budist rahipler")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/16-1.jpg)

17-Tipitaka metinlerinin bölümleri şu şekildedir: 1- Vinaya Pitaka: Sangha adı verilen bu bölüm rahip ve rahibelerle ilgili kuralları, bunların nasıl yerine getirileceğini içerir. İçinde rahip olmayan Budistlerle ilgili konular da vardır. 2- Sutta Pitaka: Buda’nın fikirlerini açıkladığı konuşmalarının çoğu bu bölümde bulunur. Bunun için bu bölüme doktrin (dhamma) sepeti de denir. Bu sözler asırlar boyunca sözlü olarak nakledilmiş, başka efsanelerle, batıl inanışlarla içiçe geçmiştir. 3- Abhidhamma Pitaka: Buda’nın vaazlarının yorumları ve Budizmin felsefesi bu bölümde yer alır.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/17-1.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/17-1.jpg)

18-Günümüz Budizminin kutsal olarak kabul ettiği kitabın adı “üç sepet” anlamına gelen Tipitaka’dır. Bu metinler Pali diliyle yazılmıştır. Tipitaka’nın ne zaman yazıya geçirildiği ise kesin olarak bilinmemektedir. Ancak MÖ 1. yüzyılda Seylan’da bugünkü şeklini aldığı ileri sürülmektedir.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/18-1.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/18-1.jpg)

19-“Buda” kelimesi “uyandırılmış” veya “aydınlanmış” anlamlarına gelir ve Siddharta Gautama’nın eriştiği varsayılan manevi dereceyi ifade etmektedir. Buda’dan günümüze ulaşan metinler ise onun yaşadığı döneme ait değildir, onun ölümünden 300 – 400 yıl sonra kaleme alınmıştır. Bu metinlerde pek çok batıl inanış, akıl ve mantıkla çelişen çarpık uygulamalar ve Buda’yı önünde secde edilen bir put gibi gösteren sapkın açıklamalar bulunmaktadır.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/19-1.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/19-1.jpg)

20-Soylu Sakya ailesine mensup olan Gautama, Suudhodana isimli asil bir prensin oğlu olarak dünyaya gelmiş, gençliğini refah ve bolluk içinde geçirmişti. 29 yaşında sarayından ayrılan Gautama, 80 yaşında hayatını yitirene kadar mistik bir arayış içine girmiş ve bazı prensipler belirlemiştir. Bu prensipler zaman içinde bir öğretiye dönüşmüştür ve “Budizm” de budur.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/20.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/20.jpg)

21-Budizm’in kurucusu Siddharta Gautama MÖ 563-483 yılları arasında Hindistan’ın Kapilavastu şehrinde yaşamıştır. Onun yaşadığı dönemde Hindistan’da yaygın din, ülkeyi işgal eden Aryaların dini olan Brahmanizmdi. Aryalar katı ve asla aşılmaz bir kast sistemi uyguluyorlardı. Bu kast düzenine göre toplum dört gruba ayrılmıştı. Her grup alt kastlara bölünüyordu. Brahman din adamları, toplumun en üst kesimini oluşturuyorlar ve halka çok acımasızca eziyette bulunuyorlardı.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/21.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/21.jpg)

22-Budizm günümüzden yaklaşık 2500 yıl önce Hindistan’ın kuzeydoğusunda ortaya çıkmış ve zaman içinde Sri Lanka, Moğolistan, Seylan, Mançurya, Kore, Japonya, Tibet, Çin, Tayland ve Nepal gibi ülkelerde etkili olmuştur. Bugün dünya üzerinde yaklaşık 300 milyon civarında Budist (ve Budist sempatizanı) olduğu tahmin edilmektedir.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/22.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/22.jpg)

23-Budizm’in sapkın inanışları bu kişiler üzerinde ciddi psikolojik tahribat oluşturur. Budizm’in yaygın olduğu ülkeler veya Budist rahiplerin yoğun olarak yaşadığı yerler incelendiğinde, söz konusu yerlere karamsarlık ve boğuculuğun hakim olduğu açıkça görülecektir.

[![budist rahip](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/23.jpg "budist rahip")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/23.jpg)

24-Oysa Budizm, sanıldığı gibi insanlara huzur getiren bir inanış değildir. Tam tersine Budizm, kendisine kapılan insanları büyük bir karamsarlığın içine çeker. Aldıkları eğitime, sahip oldukları modern dünya görüşüne rağmen bu insanlar, ellerinde kaplarla dilencilik yapmayı makul gören, insanların farelere veya ineklere dönüşeceği saçmalığına inanan, taştan yapılmış putlardan medet uman insanlara dönüşürler.

[![24](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/24.jpg)](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/24.jpg)

25-Budizm propagandası yapan çevrelerin kullandıkları bir diğer yöntem de bu batıl inanışın insanlara sözde bir kurtuluş yolu olarak sunulmasıdır. İçinde yaşadıkları materyalist toplumdan, bu toplumdaki merhametsiz ve çatışmacı kültürden, acımasızlıktan, sıkıntılardan, kargaşadan, çatışmalardan, rekabetten, bencilliklerden ve yalancılıktan kaçan insanlara Budizm, bir barış, güven, hoşgörü ve huzur dolu bir hayatın yolu olarak lanse edilmektedir.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/25.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/25.jpg)

26-Brahma diniyle, Hinduizmle, Şintoizmle ve diğer putperest Doğu dinleriyle kaynaştıkça daha da karanlık bir şekle bürünmüştür. Uzakdoğu’nun gizemli görünümünden etkilenerek, inanmadıkları halde sadece dikkat çekmek için bu batıl dini benimseyen kişiler unutmamalıdır ki, Budizm gerçekte insanı Allah’ı inkar etmeye, elle yapılan putları Allah’a şirk koşmaya ve batıl bir hayat sürmeye kadar götürebilen sapkın öğretiler içermektedir. Budizmin akıl dışı yönlerini görmezden gelip, bir özenti nedeniyle bu dini benimsemek insanı çok büyük bir kayba götürecektir.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/26.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/26.jpg)

27-Budizmin ortaya çıkışını, yazılı kaynaklarını, genel inanışlarını, ibadet şekillerini ve söz konusu dinin kurucusu Buda’nın hayatını Kuran ayetleri ile değerlendirdiğimizde, bu felsefenin temelinin sapkın öğretiler üzerine kurulu olduğunu, insan aklı ve mantığıyla çelişen garip ibadetler içerdiğini ve insanı taştan, topraktan putlara ibadet etmeye yönelttiğini görürüz. Zaten akıl ve mantıkla bağdaşmayan bir inanış olan Budizm, kabul gördüğü ülkelerin putperest anlayışıyla, gelenek ve görenekleriyle de karışmış, hurafelerle ve sapkın düşüncelerle bütünleşerek tam anlamıyla inkarcı bir felsefe halini almıştır.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/27.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/27.jpg)

28-İşte bu nedenle de toplum içinde “diğer insanlardan farklı” sıfatıyla tanınmak ve “gizemli insan” imajı vermek isteyen kişiler için Budizm önemli bir araç haline gelmiştir. Örneğin sıradan hayata sahip olan bir kişi, günün birinde saçını kazıtıp Budist kıyafetleriyle dolaşmaya başlar ve çevresindeki kişilere o güne kadar kullanmadığı mistik kelimelerle Budist öğretiyi anlatmaya başlarsa, doğal olarak dikkat çekecek, “orijinal bir insan” gibi görülecektir.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/28.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/28.jpg)

29-Örneğin Budizmin ortaya çıkışı insanlara efsanevi, uhrevi bir masal olarak anlatılır. Budizmi anlatan kitaplarda ya da filmlerde Buda büyük bir gizem kaynağı olarak gösterilir. Aynı şekilde Budist rahiplerin hayatları da Batılı insanlara sırlarla dolu, dolayısıyla dikkat çekici olarak tanıtılır. Bu kişilerin ilginç kıyafetleri, kazıtılmış saçları, ibadet şekilleri, törenleri, yaşadıkları yerler, yoga ve meditasyon gibi garip uygulamaları insanları hayrete düşürür ve ilgilerini çeker.

[![budizm](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/29.jpg "budizm")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/29.jpg)

30-Budizmi seçen insanlar da genelde bu felsefeyi inandıklarından ya da mantıklı bulduklarından değil, söz konusu “mistik” havadan etkilendikleri için seçerler. Çünkü bu batıl inanış onlara, günlük yaşamlarından, hayatları boyunca karşılaştıkları diğer felsefelerden çok daha farklı ve şaşırtıcı bir şekilde sunulur.

[![budizim](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/30.jpg "budizim")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/30.jpg)

31-Dünya üzerinde, özellikle de Amerika ve Avrupa’da bazı kimselerde, Budizme yönelik bir ilgi görülür. Bu ilginin en önemli nedenlerinden biri, bu batıl inanışın, insanlara gizemli, mistik ve hayret verici özelliklere sahip olduğu izlenimi oluşturacak şekilde lanse edilmesidir.

[![budizm heykel](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/31.jpg "budizm heykel")](https://dusuneninsanlaricin.com/wp-content/uploads/2016/11/31.jpg)

32-Son yıllarda da Batılı toplumlarda “alışılanın dışında” ve “toplum dışı” hayat tarzlarıyla dikkat çeken yeni bir akım görülmektedir. Bu akım Doğu kültürünü, felsefelerini ve inanışlarını kullanarak dikkat çekmeye çalışan kimselerden oluşmaktadır. Bu akımın kullandığı Doğu felsefelerinin başında ise Budizm gelir.
